//Función que modifica el estado de la octava fila si se selecciona una casa
$(document).ready(function(){
    $("select[name='tipoInmueble']").change(function(){
        var disableIt=$(this).val()=='2' ? false : true;
        $("#nplantas").attr("disabled", disableIt);
        $("#metrosparcela").attr("disabled", disableIt);        
    });
});

//Lectura del formulario
const form = document.forms[0];

form.addEventListener("submit", function(event) {
  event.preventDefault();
  new FormData(form);

});

//Obtención de todos los datos del formulario
form.addEventListener("formdata", event => {
  const data = event.formData;

  var tipo = data.get("tipoInmueble");

  var pisos = data.get("piso");
  if (tipo == 2){
    pisos = -5;
  } 
  let barrio = data.get("barrio");
  let distrito = data.get("distrito");
  let metros = data.get("metrosConstruidos");
  let nhab = data.get("nhabitaciones");
  let nbanos = data.get("nbanos");

  let garaje = data.get("garaje");
  if(garaje === null){
    garaje = 0;
  }
  let ascensor = data.get("ascensor");
  if(ascensor === null){
    ascensor = 0;
  }
  let exterior = data.get("exterior");
  if(exterior === null){
    exterior = 0;
  }
  let rehabilitar = data.get("rehabilitar");
  if(rehabilitar === null){
    rehabilitar = 0;
  }
  let certificado = data.get("certEnergetico");

  var nplantas = 1;
  if (tipo == 2){
    nplantas = data.get("nplantas");
  } else if (tipo == 5){
    nplantas = 2;
  }

  var mparcela = 0;
  if(tipo == 2){
    mparcela = data.get("metrosparcela");
  } 

  //Todos los datos en un tensor
  var p_new = tf.tensor2d([metros, nhab, nbanos, nplantas, mparcela, pisos, rehabilitar,
  ascensor, exterior, certificado, garaje, barrio, distrito, tipo], [1,14], 'float32');
  
  //Media y desviacion del conjunto original para normalizar los datos
  const statMean = tf.tensor2d([143.52839, 2.956889, 2.03714, 1.213589, 38.744611, 
    1.892514, 0.181838, 0.690504, 0.917419, 1.133411, 0.347364, 62.971381, 9.775051,
    1.397102], [1,14],  'float32');
  const statStd = tf.tensor2d([127.406566, 1.420341, 1.304186, 0.682423, 227.942525, 
    2.969259, 0.385725, 0.462303, 0.275258, 1.888055, 0.47615, 38.487794, 5.615234,
    1.014913], [1,14], 'float32');

  //Media y desviacion del objetivo para recuperar el valor 
  const meanPrice = 604381.7642;
  const stdPrice = 631243.007224;

  //Normalizar los datos del piso
  var p_nn = normalizar(p_new);

  const model_url = 'pisos_model/model.json';

  const init = async () => {
    //Cargar el modelo
    const model = await tf.loadLayersModel(model_url);

    //Calcular el precio y convertirlo
    var predictions = await model.predict(p_nn).data();
    predictions = desnormalizar(predictions);

    //Obtener los valores máximo y mínimo
    var p_max = predictions + (predictions * 0.2);
    var p_min = predictions - (predictions * 0.2);

    //Dar formato moneda euro
    p_max = new Intl.NumberFormat('es-ES', { style: 'currency', currency: 'EUR' }).format(p_max);
    p_min = new Intl.NumberFormat('es-ES', { style: 'currency', currency: 'EUR' }).format(p_min);
    
    //Mostrar los valores
    $("#preciomax").empty();
    $("#preciomin").empty();
    $("#preciomax").val(p_max);
    $("#preciomin").val(p_min);
    
  }
  init();

  //Funcion para normalizar los datos del piso y pasarselos al modelo
  function normalizar(arr){
    return (arr.sub(statMean)).div(statStd);
  }
  //Funcion para obtener el valor del piso
  function desnormalizar(num){
    return (num * stdPrice) + meanPrice;
  }
  
});


