# Valorar inmuebles de Madrid con Tensorflow.js
Calcular el valor de un inmueble en Madrid directamente en el navegador. Se utiliza un red neuronal desarrollada con keras que fue exportada a Tensorflow.js. 

![](img/portada.png)

## Pasos para ejecutar el programa
- `git clone https://gitlab.com/mako-ml-solutions/madrid-house-prices.git `
- `cd madrid-houses `
- `python3 -m http.server `
- Abrir el navegador en la dirección [localhost:8000](http://localhost:8000/)

## Desarrollado con
* [Keras](https://www.tensorflow.org/guide/keras)
* [Tensorflow.js](https://www.tensorflow.org/js)
* [Twitter Bootstrap](https://getbootstrap.com/)

## Licencia
El proyecto tiene una licencia descrita en [Licencia](licencia.md)